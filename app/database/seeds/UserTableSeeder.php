<?php

class UserTableSeeder extends Seeder {
	public function run()
	{
		$user = Sentry::createUser(array(
	        'email'     => '',
	        'username'     => 'admin',
	        'password'  => 'admin123123',
	        'activated' => true,
	    ));

	    // Find the group using the group id
	    $adminGroup = Sentry::findGroupById(1);

	    // Assign the group to the user
	    $user->addGroup($adminGroup);
	}
}