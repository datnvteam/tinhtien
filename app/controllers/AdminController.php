<?php 

class AdminController extends Controller {
	public function getIndex()
	{
		return View::make('admin.dashboard');
	}
	public function getUsers()
	{
		return View::make('users');
	}
}