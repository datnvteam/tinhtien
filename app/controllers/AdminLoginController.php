<?php 

class AdminLoginController extends Controller {
	public function getLogin()
	{
		
		return View::make('layouts.login');
	}
	public function postLogin()
	{
		try
		{
		    // Login credentials
		    $credentials = array(
		        'username'    => Input::get('username'),
		        'password' => Input::get('password'),
		        'activated'=>true
		    );

		    // Authenticate the user
		    $user = Sentry::authenticate($credentials, Input::get('remember', false));
		    return Redirect::to('admin');
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		    $message = 'Login field is required.';
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		    $message = 'Password field is required.';
		}
		catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
		{
		    $message = 'Wrong password, try again.';
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    $message = 'User was not found.';
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
		    $message = 'User is not activated.';
		}

		// The following is only required if the throttling is enabled
		catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
		{
		    $message = 'User is suspended.';
		}
		catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
		{
		    $message = 'User is banned.';
		}
		return Redirect::back()->withInput()->withErrors($message);
	}
	public function getLogout()
	{
		Sentry::logout();
		return Redirect::to('admin/login');
	}
}