<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>@yield('title', isset($title)?$title:'Calculator')</title>

{{ HTML::style('css/bootstrap.min.css') }}
{{ HTML::style('css/styles.css') }}

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
@yield('style')
</head>

<body>
	
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Log in</div>
				<div class="panel-body">
					@if($errors->has())
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							{{ $errors->first() }}
						</div>
					@endif
					{{ Form::open(array('method'=>'POST')) }}
						<fieldset>
							<div class="form-group">
								{{ Form::text('username', '', array('class'=>'form-control', 'autofocus', 'placeholder'=>'Username')) }}
							</div>
							<div class="form-group">
								{{ Form::text('password', '', array('class'=>'form-control', 'autofocus', 'placeholder'=>'Password')) }}
							</div>
							<div class="checkbox">
								<label>
									{{ Form::checkbox('remember', 1, array('checked'=>true)) }}
									Remember Me
								</label>
							</div>
							{{ Form::button('submit', array('class'=>'btn btn-primary', 'type'=>'submit')) }}
						</fieldset>
					{{ Form::close() }}
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	
		
	{{ HTML::script('js/jquery-1.11.1.min.js') }}
	{{ HTML::script('js/bootstrap.min.js') }}
	@yield('script')
</body>

</html>
