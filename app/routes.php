<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return '123';
});

Route::get('admin/login', array('before'=>'sentry.guest', 'uses'=>'AdminLoginController@getLogin'));
Route::post('admin/login', array('before'=>'sentry.guest', 'uses'=>'AdminLoginController@postLogin'));
Route::get('admin/logout', array('before'=>'sentry.auth', 'uses'=>'AdminLoginController@getLogout'));
Route::group(['prefix' => 'admin', 'before'=>'sentry.auth'], function() {
    Route::controller('/', 'AdminController');
});
